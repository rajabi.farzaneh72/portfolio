export default function Schema() {
  try {
    return {
        "@context": "http://schema.org",
        "@type": "Person",
        "name": "Meisam Mozafari",
        "birthDate": "1987-11-24",
        "birthPlace": {
          "@type": "Place",
          "name": "Iran"
        },
        "description": "I first started my artistic career by writing short stories and learned music and painting as a teenager. In 2015, I started writing plays under the supervision of Mohammad Charmshir, a great Iranian playwright. Also, I studied stage design at the bachelor's level and theater directing at the master's level at the university, and in this way, I learned from professors such as Dr. Masoud Delkhah (theatre director), Dr. Farhad Mohandespour (director and dramaturg), and Kianoush Ayari (cinema director) and I presented my thesis by studying the works of Jerzy Grotowski, Tadeusz Kantor, Robert Wilson, Samuel Beckett and Bertolt Brecht with an excellent grade.",
        "worksFor": {
          "@type": "EducationalOrganization",
          "name": "Name of University"
        },
        "alumniOf": {
          "@type": "EducationalOrganization",
          "name": "Name of University"
        },
        "workLocation": "Iran",
        "jobTitle": "Artist, Writer, Director, Playwright",
        "url": "Your Website or Social Media Profile URL",
        "sameAs": [
          "https://www.linkedin.com/in/meisam-mozaffari-87b9a0167/"
        ],
        "workPresented": {
            "@type": "Movie",
            "name": "DROWN OUT",
          },
      };
  } catch (error) {
  }
  return {};
}
